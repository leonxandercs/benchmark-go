package resources

type Criteria struct {
	IDProducto int64
	Anio       int16
	Mes        int8
	Moneda     string
}
