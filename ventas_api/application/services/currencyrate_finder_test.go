package services

import (
	"fmt"
	"testing"
)

var counter = 200

func BenchmarkFindAsync(b *testing.B) {
	var data map[string]float32
	dates := make([]string, 0)
	for i := 0; i < counter; i++ {
		dates = append(dates, "20121119")
	}
	finder := NewCurrencyRateFinder("http://localhost:8081/tipocambio/api/v1")
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		data, _ = finder.FindAll(dates)
		//fmt.Printf("%+v\n", data)
	}
	fmt.Printf("\nlen(data)=%d\n", len(data))
}
