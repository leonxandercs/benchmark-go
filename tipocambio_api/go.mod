module tipocambio_api

go 1.14

require (
	github.com/francoispqt/gojay v0.0.0-20181220093123-f2cc13a668ca
	github.com/francoispqt/onelog v0.0.0-20190306043706-8c2bb31b10a4
	github.com/pkg/errors v0.9.1
	github.com/valyala/fasthttp v1.9.0
	go.mongodb.org/mongo-driver v1.3.2
	gopkg.in/yaml.v2 v2.2.8
)
