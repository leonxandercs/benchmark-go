package services

import (
	"time"
	"tipocambio_api/application/resources"
)

type ITipocambioFinder interface {
	Find(date *time.Time) (*resources.Tipocambio, error)
}
