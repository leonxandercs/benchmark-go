package resources

import "github.com/francoispqt/gojay"

type Venta struct {
	Producto          string `json:"nombre_producto"`
	DescripcionMoneda string `json:"descripcion_moneda"`
	AnioMes           string `json:"aniomes"`
	MontoTotal        float64 `json:"monto_total"`
}

func (venta *Venta) MarshalJSONObject(enc *gojay.Encoder) {
	enc.StringKey("nombre_producto", venta.Producto)
	enc.StringKey("descripcion_moneda", venta.DescripcionMoneda)
	enc.StringKey("aniomes", venta.AnioMes)
	enc.Float64Key("monto_total", venta.MontoTotal)
}
func (venta *Venta) IsNil() bool {
	return venta == nil
}

type Ventas []Venta

func (t Ventas) MarshalJSONArray(enc *gojay.Encoder) {
	for _, e := range t {
		enc.AddObject(&e)
	}
}
func (t Ventas) IsNil() bool {
	return t == nil
}
