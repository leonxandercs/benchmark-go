package repository

import (
	"ventas_api/application/models"
)

type IVentaRepository interface {
	FindByCriteria(*Criteria) (*[]models.Venta, error)
}
