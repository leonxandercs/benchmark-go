package loggerpool

import (
	"github.com/francoispqt/onelog"
	"os"
	"sync"
)

var loggerPool = sync.Pool{
	New: func() interface{} {
		return onelog.New(
			os.Stdout,
			onelog.ALL,
		)
	},
}

func GetLogger() *onelog.Logger {
	return loggerPool.Get().(*onelog.Logger)
}

func End(logger *onelog.Logger) {
	logger = &onelog.Logger{}
	loggerPool.Put(logger)
}

var once sync.Once

type logPool struct {
	logger *onelog.Logger
}

func GetLoggerPool() *logPool {
	return &logPool{}
}

func (l *logPool) Logger() *onelog.Logger {
	if l.logger == nil {
		l.logger = loggerPool.Get().(*onelog.Logger)
	}

	return l.logger
}

func (l *logPool) End() {
	loggerPool.Put(l.logger)
}
