package models

import (
	"github.com/jackc/pgx/pgtype"
)

type Venta struct {
	Producto          pgtype.Varchar
	Fechaventa        pgtype.Timestamp
	Monto             pgtype.Float8
	IsoMoneda         pgtype.Varchar
	DescripcionMoneda pgtype.Varchar
}
