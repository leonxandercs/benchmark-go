package services

import (
	"github.com/francoispqt/gojay"
	"io/ioutil"
	"net/http"
	"time"
)

const defaultCurrencyRate = float32(3)

type currencyRateFinder struct {
	baseUrl string
}

func NewCurrencyRateFinder(baseUrl string) *currencyRateFinder {
	return &currencyRateFinder{baseUrl: baseUrl}
}

func (finder *currencyRateFinder) Find(date string) (float32, error) {

	url := finder.baseUrl + "/tipocambio/" + date

	c := &http.Client{Timeout: 30 * time.Second}
	req, _ := http.NewRequest("GET", url, nil)
	res, err := c.Do(req)
	if err != nil || res.StatusCode != http.StatusOK {
		return defaultCurrencyRate, nil
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return defaultCurrencyRate, nil
	}

	var tipocambioRes tipoCambioResponse
	if err := gojay.UnmarshalJSONObject(body, &tipocambioRes); err != nil {
		return defaultCurrencyRate, nil
	}

	return tipocambioRes.Tipocambio, nil
}

func (finder *currencyRateFinder) FindAll(dates []string) (map[string]float32, error) {

	var counter int
	ratesDate := make(map[string]float32, 0)

	semaphore := make(chan struct{}, 500)
	rates := make(chan struct {
		string
		float32
	})

	for _, date := range dates {

		go func(dateRate string) {
			semaphore <- struct{}{}

			rate, _ := finder.Find(dateRate)
			rates <- struct {
				string
				float32
			}{dateRate, rate}

			<-semaphore
		}(date)
	}

	for {
		select {
		case r := <-rates:
			ratesDate[r.string] = r.float32
			counter++
			if counter == len(dates) {
				close(semaphore)
				close(rates)
				return ratesDate, nil
			}
		}
	}

	return ratesDate, nil
}

type tipoCambioResponse struct {
	Tipocambio float32 `json:"tipo_cambio"`
}

// implement gojay.UnmarshalerJSONObject
func (t *tipoCambioResponse) UnmarshalJSONObject(dec *gojay.Decoder, key string) error {
	switch key {
	case "tipo_cambio":
		return dec.Float32(&t.Tipocambio)
	}
	return nil
}

func (t *tipoCambioResponse) NKeys() int {
	return 1
}
