package apihelpers

import (
	"fmt"
	"github.com/valyala/fasthttp"
	"strings"
	"tipocambio_api/application/configs"
	"tipocambio_api/application/helpers/configloader"
	"tipocambio_api/application/helpers/loggerpool"
)

var serverConf = &configs.ConfigServer{}

func init() {
	configloader.ReadConf(serverConf)
}

func ResponseError(ctx *fasthttp.RequestCtx, err error) {
	log := loggerpool.GetLogger()
	defer loggerpool.End(log)

	log.Error(fmt.Sprintf("%v", err))

	errMsg := fmt.Sprintf("ERROR %s", err)

	ctx.SetStatusCode(fasthttp.StatusInternalServerError)
	ctx.SetContentType("application/json")

	ctx.WriteString(fmt.Sprintf(`{"message":"%s"}`, errMsg))

	ctx.SetConnectionClose()
}

func IsGet(ctx *fasthttp.RequestCtx) bool {
	return string(ctx.Method()) == fasthttp.MethodGet
}

func IsPost(ctx *fasthttp.RequestCtx) bool {
	return string(ctx.Method()) == fasthttp.MethodPost
}

func GetPathParam(ctx *fasthttp.RequestCtx, path string, idxParam int8) string {

	ctxPath := strings.Replace(string(ctx.Path()), serverConf.Server.ContextPath, "", 1)

	stripedPath := strings.TrimPrefix(ctxPath, path)

	pathParams := strings.Split(stripedPath, "/")

	return pathParams[idxParam]
}
