package tests

import (
	"fmt"
	"github.com/valyala/fasthttp"
	"io/ioutil"
	"log"
	"net/http"
	"sync"
	"time"
)

var iterations = 50

func GetFasthttpTipoCambio() {

	var wg sync.WaitGroup

	for i := 0; i < iterations; i++ {
		wg.Add(1)
		go func() {
			c := &fasthttp.Client{ReadTimeout: 3 * time.Second}

			statusCode, body, err := c.Get(nil, "http://localhost:8081/tipocambio/api/v1/tipocambio/20130710")
			if err != nil {
				log.Fatalf("Error when loading google page through local proxy: %s", err)
			}
			if statusCode != fasthttp.StatusOK {
				log.Fatalf("Unexpected status code: %d. Expecting %d", statusCode, fasthttp.StatusOK)
			}

			fmt.Sprintf(string(body))

			wg.Done()
		}()
	}

	wg.Wait()
}

func GetHttpTipoCambio() {

	var wg sync.WaitGroup

	for i := 0; i < iterations; i++ {
		wg.Add(1)
		go func() {
			url := "http://localhost:8081/tipocambio/api/v1/tipocambio/20130715"

			req, _ := http.NewRequest("GET", url, nil)
			c := &http.Client{Timeout: 3 * time.Second}

			res, err := c.Do(req)
			if err != nil {
				log.Fatalf("Error when loading google page through local proxy: %s", err)
			}

			if res.StatusCode != fasthttp.StatusOK {
				log.Fatalf("Unexpected status code: %d. Expecting %d", res.StatusCode, fasthttp.StatusOK)
			}

			body, err := ioutil.ReadAll(res.Body)
			if err != nil {
				log.Fatalf("Error deserialization: %s", err)
			}
			fmt.Sprintf(string(body))

			wg.Done()
		}()
	}

	wg.Wait()
}
