package main

import (
	"tipocambio_api/application/configs"
	"tipocambio_api/application/helpers/configloader"
	"tipocambio_api/application/routers"
)

func main() {

	serverConfig := new(configs.ConfigServer)
	configloader.ReadConf(serverConfig)

	apiVentasHandler := routers.NewTipocambioHttpHandler(serverConfig)
	apiVentasHandler.ListenAndServe()

}
