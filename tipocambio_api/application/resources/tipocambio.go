package resources

import (
	"github.com/francoispqt/gojay"
	"sync"
)

type Tipocambio struct {
	Valor float32
}

func (venta *Tipocambio) MarshalJSONObject(enc *gojay.Encoder) {
	enc.Float32Key("tipo_cambio", venta.Valor)
}
func (venta *Tipocambio) IsNil() bool {
	return venta == nil
}

var tipoCambioPool = sync.Pool{New: func() interface{} {
	return new(Tipocambio)
}}

func AcquireTipoCambio() *Tipocambio {
	return tipoCambioPool.Get().(*Tipocambio)
}

func Release(t *Tipocambio) {
	t.Valor = 0
	tipoCambioPool.Put(t)
}
