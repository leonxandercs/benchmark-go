package services

import (
	"github.com/pkg/errors"
	"strconv"
	"strings"
	"time"
	"tipocambio_api/application/models"
	"tipocambio_api/application/repository"
	"tipocambio_api/application/resources"
)

type tipocambioFinder struct {
	repo repository.ITipocambioRepository
}

func NewTipocambioFinder(repo repository.ITipocambioRepository) ITipocambioFinder {
	return &tipocambioFinder{repo: repo}
}

func (finder *tipocambioFinder) Find(date *time.Time) (*resources.Tipocambio, error) {

	tipocambio, err := finder.repo.FindByDate(date)
	if err != nil {
		return nil, err
	}
	defer models.Release(tipocambio)

	valor, err := parseTipoCambio(tipocambio.Valor)
	if err != nil {
		return nil, err
	}

	tipocambioResponse := resources.AcquireTipoCambio()
	tipocambioResponse.Valor = valor

	return tipocambioResponse, nil
}

func parseTipoCambio(str string) (float32, error) {

	if strings.IndexByte(str, ',') > -1 {
		str = strings.Replace(str, ",", ".", 1)
	}

	if value, err := strconv.ParseFloat(str, 32); err == nil {
		return float32(value), err
	}

	return 0, errors.Errorf("tipo de cambio invalido (%s)", str)
}
