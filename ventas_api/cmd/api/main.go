package main

import (
	"ventas_api/application/configs"
	"ventas_api/application/helpers/configloader"
	"ventas_api/application/routers"
)

func main() {

	serverConfig := new(configs.ConfigServer)
	configloader.ReadConf(serverConfig)

	apiVentasHandler := routers.NewApiVentasHttpHandler(serverConfig)
	apiVentasHandler.ListenAndServe()

}
