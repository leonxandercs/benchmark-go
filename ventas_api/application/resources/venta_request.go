package resources

import (
	"github.com/francoispqt/gojay"
	"github.com/pkg/errors"
	"strings"
)

type VentaRequest struct {
	IDProducto int64  `json:"id_producto"`
	Anio       int16  `json:"anio"`
	Mes        int8   `json:"mes"`
	Moneda     string `json:"moneda"`
	Usuario    string `json:"usuario_consulta"`
}

func (venta *VentaRequest) Validate() error {
	errs := make([]string, 0)

	if venta.Anio != 0 && (venta.Anio < 2000 || venta.Anio > 2020) {
		errs = append(errs, "el año debe ser 0 o un valor entre 2000 y 2020")
	}

	if venta.Mes != 0 && venta.Mes > 12 {
		errs = append(errs, "el mes debe ser 0 o un valor entre 1 y 12")
	}

	if len(errs) > 0 {
		return errors.Errorf(strings.Join(errs, ",\t"))
	}
	return nil
}

// implement method encoder
func (venta *VentaRequest) MarshalJSONObject(enc *gojay.Encoder) {
	enc.Int64Key("id_producto", venta.IDProducto)
	enc.Int16Key("anio", venta.Anio)
	enc.Int8Key("mes", venta.Mes)
	enc.StringKey("moneda", venta.Moneda)
	enc.StringKey("usuario_consulta", venta.Usuario)
}
func (venta *VentaRequest) IsNil() bool {
	return venta == nil
}

// implement gojay.UnmarshalerJSONObject
func (venta *VentaRequest) UnmarshalJSONObject(dec *gojay.Decoder, key string) error {
	switch key {
	case "id_producto":
		return dec.Int64(&venta.IDProducto)
	case "anio":
		return dec.Int16(&venta.Anio)
	case "mes":
		return dec.Int8(&venta.Mes)
	case "moneda":
		return dec.String(&venta.Moneda)
	case "usuario_consulta":
		return dec.String(&venta.Usuario)
	}
	return nil
}
func (venta *VentaRequest) NKeys() int {
	return 5
}