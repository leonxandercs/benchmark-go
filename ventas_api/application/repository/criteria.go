package repository

import (
	"fmt"
)

type Criteria struct {
	IDProducto int64
	Anio       int16
	Mes        int8
	Moneda     string
}

/*
	querySql := `select
	p.descripcion_producto as producto,
	v.fecha as  fechaventa,
	v.monto,
	m.iso_moneda,
	m.descripcion_moneda
	from tabla_ventas v
	join tabla_producto p
	on v.id_producto=p.id_producto
	join tabla_moneda m
	on v.id_moneda=m.id_moneda
	where
	p.id_producto=1
	and to_char(v.fecha,'YYYY')='2015'
	and to_char(v.fecha,'MM')='12';`
*/
func (criteria *Criteria) BuildQuery() string {

	querySql := fmt.Sprintf(`select
		p.descripcion_producto as producto,
		v.fecha as  fechaventa,
		v.monto,
		m.iso_moneda,
		m.descripcion_moneda 
		from tabla_ventas v 
		join tabla_producto p
		on v.id_producto=p.id_producto
		join tabla_moneda m
		on v.id_moneda=m.id_moneda
		where p.id_producto=%d
		`, criteria.IDProducto)

	if criteria.Anio != 0 {
		querySql += fmt.Sprintf("\tand to_char(v.fecha,'YYYY')='%d'", criteria.Anio)
	}

	if criteria.Mes != 0 {
		querySql += fmt.Sprintf("\tand to_char(v.fecha,'MM')='%0*d'", 2, criteria.Mes)
	}

	return querySql
}
