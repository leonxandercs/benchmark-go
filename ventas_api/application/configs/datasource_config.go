package configs

import "ventas_api/application/helpers/configloader"

type DatasourceConfig struct {
	Database struct {
		Host     string `yaml:"host"`
		Port     int16  `yaml:"port"`
		User     string `yaml:"user"`
		Password string `yaml:"password"`
		DbName   string `yaml:"dbName"`
	} `yaml:"datasource"`
}

func (conf *DatasourceConfig) Merge(envCfg interface{}) configloader.ConfigurationProperties {
	envConfig := envCfg.(*DatasourceConfig)
	conf.Database.Host = configloader.GetVal(envConfig.Database.Host, conf.Database.Host).(string)
	conf.Database.Port = configloader.GetVal(envConfig.Database.Port, conf.Database.Port).(int16)
	conf.Database.User = configloader.GetVal(envConfig.Database.User, conf.Database.User).(string)
	conf.Database.Password = configloader.GetVal(envConfig.Database.Password, conf.Database.Password).(string)
	conf.Database.DbName = configloader.GetVal(envConfig.Database.DbName, conf.Database.DbName).(string)
	return conf
}
