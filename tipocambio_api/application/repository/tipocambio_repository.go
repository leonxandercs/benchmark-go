package repository

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
	"tipocambio_api/application/helpers/database"
	"tipocambio_api/application/models"
)

const collectionTipoCambio = "coleccion_tipo_cambio"

type tipocambioRepository struct {
	helper *database.MongodbHelper
}

func NewVentaSqlRepsitory(sqlHelper *database.MongodbHelper) ITipocambioRepository {
	return &tipocambioRepository{helper: sqlHelper}
}

func (repo *tipocambioRepository) FindByDate(date *time.Time) (*models.Tipocambio, error) {

	err := repo.helper.OpenConnection()
	if err != nil {
		return nil, err
	}

	ctx := context.Background()
	findOptions := options.FindOne()
	findOptions.SetProjection(bson.M{"_id": 0, "tipo_cambio": 1})

	filter := bson.M{"fecha": date.Format("02/01/2006")}

	tipocambio := models.AcquireTipoCambio()
	err = repo.helper.Collection(collectionTipoCambio).FindOne(ctx, filter, findOptions).Decode(&tipocambio)
	if err != nil {
		return nil, err
	}

	return tipocambio, nil
}
