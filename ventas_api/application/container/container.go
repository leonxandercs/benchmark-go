package container

import (
	"ventas_api/application/configs"
	"ventas_api/application/controllers"
	"ventas_api/application/helpers/configloader"
	"ventas_api/application/helpers/database"
	"ventas_api/application/repository"
	"ventas_api/application/services"
)

func dbConfig() *database.DBConfig {
	dbConf := new(configs.DatasourceConfig)
	configloader.ReadConf(dbConf)

	return &database.DBConfig{
		Host:     dbConf.Database.Host,
		Port:     dbConf.Database.Port,
		User:     dbConf.Database.User,
		Password: dbConf.Database.Password,
		DbName:   dbConf.Database.DbName,
	}
}

func sqlHelper() *database.SqlHelper {
	sqlHelper := database.NewSqlPgxHelper(dbConfig())
	go sqlHelper.OpenConnection()
	return sqlHelper
}

func VentaRepository() repository.IVentaRepository {
	return repository.NewVentaSqlRepsitory(sqlHelper())
}

func CurrencyExchangeFinder() services.ICurrencyRateFinder {
	rateCfg := new(configs.RateConfig)
	configloader.ReadConf(rateCfg)
	return services.NewCurrencyRateFinder(rateCfg.Rate.Url)
}

func VentaFinder() services.IVentaFinder {
	return services.NewVentaFinder(VentaRepository(), CurrencyExchangeFinder())
}

func VentaController() *controllers.VentaController {
	return controllers.NewVentaController(VentaFinder())
}
