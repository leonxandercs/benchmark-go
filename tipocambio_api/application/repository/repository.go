package repository

import (
	"time"
	"tipocambio_api/application/models"
)

type ITipocambioRepository interface {
	FindByDate(date *time.Time) (*models.Tipocambio, error)
}
