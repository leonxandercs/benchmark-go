package routers

import (
	"flag"
	"fmt"
	"github.com/valyala/fasthttp"
	"log"
	"net"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"ventas_api/application/apihelpers"
	"ventas_api/application/configs"
	"ventas_api/application/container"
	"ventas_api/application/controllers"
)

type apiVentasHttpHandler struct {
	config          *configs.ConfigServer
	ventaController *controllers.VentaController
}

func NewApiVentasHttpHandler(config *configs.ConfigServer) *apiVentasHttpHandler {
	return &apiVentasHttpHandler{config: config, ventaController: container.VentaController()}
}

func (handler *apiVentasHttpHandler) HandleFastHttp(ctx *fasthttp.RequestCtx) {

	ctxPath := strings.Replace(string(ctx.Path()), handler.config.Server.ContextPath, "", 1)

	switch ctxPath {
	case "/lookup":
		if apihelpers.IsPost(ctx) {
			handler.ventaController.GetAll(ctx)
		} else {
			ctx.Error("Unsupported path", fasthttp.StatusMethodNotAllowed)
		}
	default:
		log.Println(ctxPath)
		ctx.Error("Unsupported path", fasthttp.StatusNotFound)
	}

}

func (handler *apiVentasHttpHandler) ListenAndServe() {
	addr := fmt.Sprintf(":%d", handler.config.Server.Port)

	prefork := flag.Bool("prefork", false, "use prefork")
	child := flag.Bool("child", false, "is child proc")
	flag.Parse()

	server := &fasthttp.Server{
		Handler: handler.HandleFastHttp,
		Name:    "go",
	}

	var ln net.Listener
	if *prefork {
		ln = DoPrefork(*child, addr)
	} else {
		ln = GetListener(addr)
	}

	if err := server.Serve(ln); err != nil {
		log.Fatalf("Error when serving incoming connections: %s", err)
	}
}


func NumCPU() int {
	n := runtime.NumCPU()
	if n == 0 {
		n = 8
	}
	return n
}

func GetListener(listenAddr string) net.Listener {
	ln, err := net.Listen("tcp4", listenAddr)
	if err != nil {
		log.Fatal(err)
	}
	return ln
}

func DoPrefork(child bool, toBind string) net.Listener {
	var listener net.Listener
	if !child {
		addr, err := net.ResolveTCPAddr("tcp", toBind)
		if err != nil {
			log.Fatal(err)
		}
		tcplistener, err := net.ListenTCP("tcp", addr)
		if err != nil {
			log.Fatal(err)
		}
		fl, err := tcplistener.File()
		if err != nil {
			log.Fatal(err)
		}
		len := NumCPU()
		if len > 1 {
			len -= 1
		}

		children := make([]*exec.Cmd, len)
		for i := range children {
			children[i] = exec.Command(os.Args[0], "-prefork", "-child")
			children[i].Stdout = os.Stdout
			children[i].Stderr = os.Stderr
			children[i].ExtraFiles = []*os.File{fl}
			if err := children[i].Start(); err != nil {
				log.Fatal(err)
			}
		}
		for _, ch := range children {
			if err := ch.Wait(); err != nil {
				log.Print(err)
			}
		}
		os.Exit(0)
	} else {
		runtime.GOMAXPROCS(1)

		var err error
		listener, err = net.FileListener(os.NewFile(3, ""))
		if err != nil {
			log.Fatal(err)
		}
	}
	return listener
}