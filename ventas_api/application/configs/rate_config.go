package configs

import (
	"ventas_api/application/helpers/configloader"
)

type RateConfig struct {
	Rate struct {
		Url string `yaml:url`
	} `yaml:rate`
}

func (conf *RateConfig) Merge(envCfg interface{}) configloader.ConfigurationProperties {
	envConfig := envCfg.(*RateConfig)
	conf.Rate.Url = configloader.GetVal(envConfig.Rate.Url, conf.Rate.Url).(string)
	return conf
}
