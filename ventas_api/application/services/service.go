package services

import (
	"ventas_api/application/resources"
)

type IVentaFinder interface {
	Find(criteria *resources.Criteria) (*resources.Ventas, error)
}

type ICurrencyRateFinder interface {
	Find(date string) (float32, error)
	FindAll(dates []string) (map[string]float32, error)
}
