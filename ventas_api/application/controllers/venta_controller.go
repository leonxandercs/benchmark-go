package controllers

import (
	"github.com/francoispqt/gojay"
	"github.com/valyala/fasthttp"
	"time"
	"ventas_api/application/apihelpers"
	"ventas_api/application/helpers/loggerpool"
	"ventas_api/application/resources"
	"ventas_api/application/services"
)

type VentaController struct {
	finder services.IVentaFinder
}

func NewVentaController(finder services.IVentaFinder) *VentaController {
	return &VentaController{finder: finder}
}

func (controller *VentaController) GetAll(ctx *fasthttp.RequestCtx) {
	log := loggerpool.GetLogger()
	defer loggerpool.End(log)

	var ventaReq resources.VentaRequest
	if err := gojay.UnmarshalJSONObject(ctx.PostBody(), &ventaReq); err != nil {
		apihelpers.ResponseError(ctx, err)
		return
	}

	log.InfoWith("").Object("request", &ventaReq).Write()

	if err := ventaReq.Validate(); err != nil {
		apihelpers.ResponseError(ctx, err)
		return
	}

	ventas, err := controller.finder.Find(&resources.Criteria{
		IDProducto: ventaReq.IDProducto,
		Anio:       ventaReq.Anio,
		Mes:        ventaReq.Mes,
		Moneda:     ventaReq.Moneda,
	})

	if err != nil {
		apihelpers.ResponseError(ctx, err)
		return
	}

	body, err := gojay.MarshalJSONArray(ventas)
	if err != nil {
		apihelpers.ResponseError(ctx, err)
		return
	}

	log.InfoWith("").
		String("time", time.Now().Format(time.RFC3339)).
		Array("response", ventas).
		Write()

	ctx.SetContentType("application/json")
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.Response.SetBody(body)
	ctx.SetConnectionClose()
}
