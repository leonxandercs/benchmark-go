# Poc Benchmark api in Go 1.14 #


#### Resumen: ####
* Este repositorio contiende los dos proyectos para el benchmark, desarrollados en el lenguaje GO v1.14
utilizando "go modules", se esta utilizando el paquete [fasthttp](https://github.com/valyala/fasthttp) para optimizar 
las peticiones del servidor htttp.

* Version 1.0.0

##### Paquetes/Dependencias: #####
* [fasthttp](https://github.com/valyala/fasthttp): paquete http para go de alto rendimiento.
* [onelog](https://github.com/francoispqt/onelog): paquete para logs de alto rendimiento.
* [gojay](https://github.com/francoispqt/gojay) : paquete para encode/decode json.

### Project Structure ###
La estructura del proyecto esta insperado en "Clean Architecture" y [la estructura estandar](https://github.com/golang-standards/project-layout) de Go.


![step2](img/project_layout.png)

#### Como lo ejecuto ? ####
- Ejecutar el siguiente comando desde la raiz del proyecto:
#####Importante! #
 ejecuar el comando con el flag --compatibility tal como se muestra en el ejemplo 
 para que se aplique la configuracion de recursos.
```
#!bash

docker-compose --compatibility up --build -d

```
![docker](img/dockercompose_up.png)



###### Author: ######
* Noel Chavez Simbron

[lexers]: http://pygments.org/docs/lexers/
[fireball]: http://daringfireball.net/projects/markdown/ 
[Pygments]: http://pygments.org/ 
[Extra]: http://michelf.ca/projects/php-markdown/extra/
[id]: http://example.com/  "Optional Title Here"
[BBmarkup]: https://confluence.atlassian.com/x/xTAvEw