package configloader

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"strings"
	"tipocambio_api/application/helpers/environment"
)

const configDir = "config"
const prefixConfigFile = "conf"

var configProps = make(map[string]ConfigurationProperties)

//Eager Load
func Load(cfgs ...ConfigurationProperties) {
	configFiles := loadConfigs(configProps, cfgs...)

	fmt.Printf("Loaded configs file: [%s]\n", strings.Join(*configFiles, ","))
}

// Lazy Load
func ReadConf(conf ConfigurationProperties) {

	typeName := reflect.TypeOf(conf).Elem().Name()
	config := configProps[typeName]

	if config == nil {
		fmt.Printf("Load Config:[%s]\n", typeName)

		configFiles := loadConfigs(configProps, conf)
		config = configProps[typeName]
		fmt.Printf("Loaded config file: [%s]\n", strings.Join(*configFiles, ","))
	} else {
		fmt.Printf("Read Config:[%s]\n", typeName)
	}

	src := reflect.ValueOf(config).Elem()
	dst := reflect.ValueOf(conf).Elem()
	dst.Set(src)

}

func loadConfigs(configProps map[string]ConfigurationProperties, cfgs ...ConfigurationProperties) *[]string {

	var existEnvFile bool
	configNames := make([]string, 0)
	baseConfFileName, envConfFileName := getConfigFileNames()

	for _, cfg := range cfgs {

		unmarshalConf(baseConfFileName, cfg)

		if envConfFileName != "" {
			envConfig := cfg
			unmarshalConf(envConfFileName, envConfig)

			cfg = cfg.Merge(envConfig)
			existEnvFile = true
		}

		typeName := reflect.TypeOf(cfg).Elem().Name()
		configProps[typeName] = cfg
	}

	configNames = append(configNames, baseConfFileName)

	if existEnvFile {
		configNames = append(configNames, envConfFileName)
	}

	return &configNames
}

func unmarshalConf(configFile string, configuration ConfigurationProperties) {

	yamlFile, err := ioutil.ReadFile(configFile)
	if err != nil {
		log.Fatalf("error reading yaml file   #%v ", err)
	}

	err = yaml.Unmarshal(yamlFile, configuration)
	if err != nil {
		log.Fatalf("Unmarshal: %v", err)
	}

}

func getConfigFileNames() (string, string) {

	env := environment.GetEnv()

	yamlFileName := fmt.Sprintf("%s/%s.yaml", configDir, prefixConfigFile)
	yamlFileNameEnv := fmt.Sprintf("%s/%s-%s.yaml", configDir, prefixConfigFile, env)

	if _, err := os.Stat(yamlFileNameEnv); os.IsNotExist(err) {
		yamlFileNameEnv = ""
	}

	return yamlFileName, yamlFileNameEnv
}

func GetVal(value interface{}, defaultValue interface{}) interface{} {
	if value == nil || value == "" || value == 0 || value == false {
		return defaultValue
	}
	return value
}

type ConfigurationProperties interface {
	Merge(envCfg interface{}) ConfigurationProperties
}
