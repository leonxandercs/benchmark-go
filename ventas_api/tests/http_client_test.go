package tests

import (
	"testing"
)

func BenchmarkFashttpClient(b *testing.B) {
	for i := 0; i < b.N; i++ {
		GetFasthttpTipoCambio()
	}
}

func BenchmarkHttpClient(b *testing.B) {
	for i := 0; i < b.N; i++ {
		GetHttpTipoCambio()
	}
}
