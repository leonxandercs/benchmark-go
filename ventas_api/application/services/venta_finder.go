package services

import (
	"ventas_api/application/models"
	"ventas_api/application/repository"
	"ventas_api/application/resources"
)

const exchangeRateDateLayout = "20060102"
const productDateLayout = "01/2006"

type ventaFinder struct {
	repo       repository.IVentaRepository
	rateFinder ICurrencyRateFinder
}

func NewVentaFinder(repo repository.IVentaRepository, currencyExchange ICurrencyRateFinder) IVentaFinder {
	return &ventaFinder{repo: repo, rateFinder: currencyExchange}
}

func (finder *ventaFinder) Find(criteria *resources.Criteria) (*resources.Ventas, error) {

	data, err := finder.repo.FindByCriteria(&repository.Criteria{
		IDProducto: criteria.IDProducto,
		Anio:       criteria.Anio,
		Mes:        criteria.Mes,
		Moneda:     criteria.Moneda,
	})
	if err != nil {
		return nil, err
	}

	groupedByDates := make(map[string]struct{}, 0)
	groupedByProductoDate := make(map[KeyGroup][]models.Venta, 0)

	for _, venta := range *data {

		dateVenta := venta.Fechaventa.Time

		date := dateVenta.Format(exchangeRateDateLayout)
		productDateKey:=KeyGroup{
			producto: venta.Producto.String,
			date:     dateVenta.Format(productDateLayout),
		}

		groupedByDates[date] = struct{}{}
		groupedByProductoDate[productDateKey] = append(groupedByProductoDate[productDateKey], venta)
	}

	i := 0
	dates := make([]string, len(groupedByDates))
	for date := range groupedByDates {
		dates[i] = date
		i++
	}

	currencyRateDates, err := finder.rateFinder.FindAll(dates)
	if err != nil {
		return nil, err
	}

	ventas := mapToVenta(&groupedByProductoDate, currencyRateDates, criteria.Moneda)

	return ventas, nil
}

func mapToVenta(groupedByProductoDate *map[KeyGroup][]models.Venta, currencyRateDates map[string]float32, moneda string) *resources.Ventas {

	ventas := make(resources.Ventas, len(*groupedByProductoDate))

	i := 0
	for productDateKey, ventasGrouped := range *groupedByProductoDate {
		var montoTotal float64

		for _, v := range ventasGrouped {
			rate := currencyRateDates[v.Fechaventa.Time.Format(exchangeRateDateLayout)]

			montoMoneda := exchangeCurrencyRate(v.Monto.Float, v.IsoMoneda.String, moneda, rate)
			montoTotal += montoMoneda
		}

		ventas[i] = resources.Venta{
			Producto:          productDateKey.producto,
			DescripcionMoneda: moneda,
			AnioMes:           productDateKey.date,
			MontoTotal:        montoTotal,
		}

		i++
	}

	return &ventas
}

func exchangeCurrencyRate(monto float64, srcMoneda string, dstMoneda string, rate float32) float64 {

	if srcMoneda == "USD" && dstMoneda == "PEN" {
		return monto * float64(rate)
	}
	if srcMoneda == "PEN" && dstMoneda == "USD" {
		return monto / float64(rate)
	}

	return monto
}

type KeyGroup struct {
	producto string
	date     string
}