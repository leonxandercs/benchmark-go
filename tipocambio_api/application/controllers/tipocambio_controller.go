package controllers

import (
	"github.com/francoispqt/gojay"
	"github.com/valyala/fasthttp"
	"time"
	"tipocambio_api/application/apihelpers"
	"tipocambio_api/application/helpers/loggerpool"
	"tipocambio_api/application/resources"
	"tipocambio_api/application/services"
)

const (
	exchangeRateDateLayout = "20060102"
	TipocambioPath         = "/tipocambio/"
)

type TipocambioController struct {
	finder services.ITipocambioFinder
}

func NewTipocambioController(finder services.ITipocambioFinder) *TipocambioController {
	return &TipocambioController{finder: finder}
}

func (controller *TipocambioController) Get(ctx *fasthttp.RequestCtx) {
	log := loggerpool.GetLogger()
	defer loggerpool.End(log)

	dateStr := apihelpers.GetPathParam(ctx, TipocambioPath, 0)

	log.InfoWith("").String("time", time.Now().Format(time.RFC3339)).String("requestDate", dateStr).Write()

	date, err := time.Parse(exchangeRateDateLayout, dateStr)
	if err != nil {
		apihelpers.ResponseError(ctx, err)
		return
	}

	tipocambio, err := controller.finder.Find(&date)
	if err != nil {
		apihelpers.ResponseError(ctx, err)
		return
	}
	defer resources.Release(tipocambio)


	enc := gojay.BorrowEncoder(ctx.Response.BodyWriter())
	defer enc.Release()

	if err:=enc.EncodeObject(tipocambio);err != nil {
		apihelpers.ResponseError(ctx, err)
		return
	}

	log.InfoWith("").
		String("time", time.Now().Format(time.RFC3339)).
		Object("response", tipocambio).
		Write()

	ctx.SetContentType("application/json")
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetConnectionClose()
}
