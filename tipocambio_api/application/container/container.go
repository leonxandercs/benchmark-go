package container

import (
	"tipocambio_api/application/configs"
	"tipocambio_api/application/controllers"
	"tipocambio_api/application/helpers/configloader"
	"tipocambio_api/application/helpers/database"
	"tipocambio_api/application/repository"
	"tipocambio_api/application/services"
)

func dbConfig() *database.MongoConfig {
	dbConf := &configs.MongoConfig{}
	configloader.ReadConf(dbConf)

	return &database.MongoConfig{
		Uri:             dbConf.Mongodb.Uri,
		User:            dbConf.Mongodb.Credentials.Username,
		Password:        dbConf.Mongodb.Credentials.Pasword,
		Database:        dbConf.Mongodb.DatabaseName,
		ApplicationName: dbConf.Mongodb.ApplicationName,
		MinPoolSize:     dbConf.Mongodb.ConnectionPool.MinSize,
		MaxPoolSize:     dbConf.Mongodb.ConnectionPool.MaxSize,
		AuthMechanism:   dbConf.Mongodb.AuthMechanism,
	}
}

func mongodbHelper() *database.MongodbHelper {
	mongodbHelper := database.NewMongodbHelper(dbConfig())
	go mongodbHelper.OpenConnection()
	return mongodbHelper
}

func TipocambioRepository() repository.ITipocambioRepository {
	return repository.NewVentaSqlRepsitory(mongodbHelper())
}

func TipocambioFinder() services.ITipocambioFinder {
	return services.NewTipocambioFinder(TipocambioRepository())
}

func TipocambioController() *controllers.TipocambioController {
	return controllers.NewTipocambioController(TipocambioFinder())
}
