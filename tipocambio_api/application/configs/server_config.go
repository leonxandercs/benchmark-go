package configs

import "tipocambio_api/application/helpers/configloader"

type ConfigServer struct {
	Server struct {
		Port        int32  `yaml:"port"`
		ContextPath string `yaml:"contextPath"`
	} `yaml:"server"`
}

func (conf *ConfigServer) Merge(envCfg interface{}) configloader.ConfigurationProperties {
	envConfig := envCfg.(*ConfigServer)
	conf.Server.Port = configloader.GetVal(envConfig.Server.Port, conf.Server.Port).(int32)
	conf.Server.ContextPath = configloader.GetVal(envConfig.Server.ContextPath, conf.Server.ContextPath).(string)
	return conf
}
