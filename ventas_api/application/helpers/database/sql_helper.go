package database

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"log"
	"sync"
)

var once sync.Once

type DBConfig struct {
	Host     string
	Port     int16
	User     string
	Password string
	DbName   string
}

type SqlHelper struct {
	db   *pgxpool.Pool
	conf *DBConfig
}

func NewSqlPgxHelper(conf *DBConfig) *SqlHelper {
	return &SqlHelper{conf: conf}
}

func (repo *SqlHelper) OpenConnection() *pgxpool.Pool {

	once.Do(func() {
		db, err := pgxpool.Connect(context.Background(), repo.GetDsn())
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Successfully connected!")
		fmt.Println(".......................")
		repo.db = db
	})

	return repo.db
}

func (repo *SqlHelper) GetDsn() string {

	return fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s pool_min_conns=%d pool_max_conns=%d sslmode=disable",
		repo.conf.Host, repo.conf.Port, repo.conf.User, repo.conf.Password, repo.conf.DbName, 100, 500)
}
