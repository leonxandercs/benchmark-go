module ventas_api

go 1.14

require (
	github.com/francoispqt/gojay v0.0.0-20181220093123-f2cc13a668ca
	github.com/francoispqt/onelog v0.0.0-20190306043706-8c2bb31b10a4
	github.com/jackc/pgtype v1.3.0
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/jackc/pgx/v4 v4.6.0
	github.com/pkg/errors v0.9.1
	github.com/valyala/fasthttp v1.9.0
	gopkg.in/yaml.v2 v2.2.8
)
