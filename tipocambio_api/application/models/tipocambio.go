package models

import "sync"

type Tipocambio struct {
	Valor string `bson:"tipo_cambio"`
}

var tipoCambioPool = sync.Pool{New: func() interface{} {
	return new(Tipocambio)
}}

func AcquireTipoCambio() *Tipocambio {
	return tipoCambioPool.Get().(*Tipocambio)
}

func Release(t *Tipocambio) {
	t.Valor = ""
	tipoCambioPool.Put(t)
}
