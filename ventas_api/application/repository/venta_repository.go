package repository

import (
	"context"
	"ventas_api/application/helpers/database"
	"ventas_api/application/models"
)

type ventaSqlRepository struct {
	sqlHelper *database.SqlHelper
}

func NewVentaSqlRepsitory(sqlHelper *database.SqlHelper) IVentaRepository {
	return &ventaSqlRepository{sqlHelper: sqlHelper}
}

func (repo *ventaSqlRepository) FindByCriteria(criteria *Criteria) (*[]models.Venta, error) {

	db := repo.sqlHelper.OpenConnection()

	rows, err := db.Query(context.Background(), criteria.BuildQuery())
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	ventas := make([]models.Venta, 0)
	venta := new(models.Venta)

	for rows.Next() {

		if err = rows.
			Scan(
				&venta.Producto,
				&venta.Fechaventa,
				&venta.Monto,
				&venta.IsoMoneda,
				&venta.DescripcionMoneda,
			);
			err != nil {
			return nil, err
		}

		ventas = append(ventas, *venta)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return &ventas, nil
}
