package resources

type VentaResponse struct {
	Producto          string  `json:"nombre_producto"`
	DescripcionMoneda string  `json:"descripcion_moneda"`
	AnioMes           string  `json:"aniomes"`
	MontoTotal        float64 `json:"monto_total"`
}
